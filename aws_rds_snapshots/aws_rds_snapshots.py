import boto3
from datetime import datetime, timezone, date
from botocore.exceptions import ClientError


class SnapshotProcessor:

    def __init__(self, rds):
        self.rds = rds

    def create_snapshot(self, db_name, snapshot_type):

        # Constructure snapshot name as <db_name>-<snapshot_type>-<timestamp>
        current_time_string = datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
        snapshot_name = "{0}-{1}-{2}".format(db_name, snapshot_type, current_time_string)

        # Create snapshot
        try:
            self.rds.create_db_snapshot(
                DBSnapshotIdentifier=snapshot_name,
                DBInstanceIdentifier=db_name,
                Tags=[{
                    'Key': 'Resources',
                    'Value': 'Backup'
                }]
            )
            print("Snapshot {0} created.".format(snapshot_name))

        # Catch error
        except ClientError as e:
            if e.response['Error']['Code'] == 'InvalidDBInstanceState':
                print("Snapshot creation was already initiated.")
            else:
                print("Undefined error: {}".format(e))

        print("")

    def delete_snapshot(self, snapshots, snapshot_name_prefix, retention_days):

        # Delete <snapshot_name_prefix> snapshots older than <retention_days>
        current_datetime = datetime.now(timezone.utc)
        current_date_string = current_datetime.strftime("%Y-%m-%d")

        for snapshot in snapshots:
            snapshot_name = snapshot['DBSnapshotIdentifier']

            # Skip snapshots that do not match the prefix
            if not snapshot_name.startswith(snapshot_name_prefix):
                continue

            # Skip unfinished snapshots that may not have SnapshotCreateTime yet
            if current_date_string in snapshot_name:
                continue

            snapshot_age = current_datetime - snapshot['SnapshotCreateTime']
            print("Snapshot {0} is {1} days old.".format(snapshot_name, snapshot_age.days))

            # Delete snapshot older than <retention_days>
            if snapshot_age.days > retention_days:
                self.rds.delete_db_snapshot(DBSnapshotIdentifier=snapshot_name)
                print("Snapshot {0} deleted.".format(snapshot_name))

        print("")


if __name__ == "__main__":

    # Define variables
    regions = ['ap-southeast-1']
    weekly_snapshot_retention_days = 28
    monthly_snapshot_retention_days = 183
    migration_snapshot_retention_days = 7

    for region in regions:
        rds = boto3.client('rds', region_name=region)
        dbs = rds.describe_db_instances()['DBInstances']
        snapshot_processor = SnapshotProcessor(rds)

        for db in dbs:
            db_name = db['DBInstanceIdentifier']
            print(db_name)

            # Create monthly snapshot on first date of the month
            if int(datetime.now().strftime("%d")) == 1:
                print("Monthly snapshot:")
                snapshot_processor.create_snapshot(db_name, "monthly")

            # Otherwise create weekly snapshot on every Saturday
            elif int(date.today().weekday()) == 5:
                print("Weekly snapshot:")
                snapshot_processor.create_snapshot(db_name, "weekly")

            # Get all snapshots
            snapshots = snapshot_processor.rds.describe_db_snapshots(
                DBInstanceIdentifier=db_name
            )['DBSnapshots']

            # Delete weekly snapshots older than 28 days
            print("Remove weekly snapshots older than 28 days:")
            weekly_snapshot_name_prefix = "{0}-weekly-".format(db_name)
            snapshot_processor.delete_snapshot(
                snapshots, 
                weekly_snapshot_name_prefix, 
                weekly_snapshot_retention_days
            )

            # Delete monthly snapshots older than 183 days
            print("Remove monthly snapshots older than 183 days:")
            monthly_snapshot_name_prefix = "{0}-monthly-".format(db_name)
            snapshot_processor.delete_snapshot(
                snapshots, 
                monthly_snapshot_name_prefix, 
                monthly_snapshot_retention_days
            )

            print("---------------------------------------------------------")
